import { Component } from '@angular/core';
import { TasksService } from './tasks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Calendar';
    company_name = 'Company name';
    user_name = 'John Doe';
    referenceDate = [new Date(), new Date(), new Date()];
    calendarContent: Array<string | number> = [];
    viewStyle = 'm';
    ats = [];
    atsv = null;
    newText = null;
    allloggers = [];
    loggers = [];
    showExits = [];
    times = [22,22,23,23];
    selectedDate = new Date();
    extraModels = [];
    errorMsg = '';
    private calcDates() {
        this.calendarContent = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
	let tmp2 = [];
            this.referenceDate[1] = new Date(this.referenceDate[0].getFullYear(), this.referenceDate[0].getMonth(), 1);
	    this.referenceDate[2] = new Date(this.referenceDate[0].getFullYear(), this.referenceDate[0].getMonth(),0);
	    let t = new Date(this.referenceDate[0].getFullYear(), this.referenceDate[0].getMonth()+1,0);
	    let length = new Date(this.referenceDate[0].getFullYear(), this.referenceDate[0].getMonth()-1,0).getUTCDay() - this.referenceDate[1].getUTCDay();
	    let length2 = new Date(this.referenceDate[2].getFullYear(), this.referenceDate[2].getMonth()+1,0).getUTCDate()+1;
	    console.log(t);
	    console.log(this.referenceDate);
	    let tmp = Array.from(Array(t.getUTCDate()+1), (_, i) => i+1);
	    if(this.referenceDate[1].getUTCDay() > 0) {
	        let l2 = this.referenceDate[2].getUTCDay()+1;
		let preextension = Array.from(Array(l2), (_,i) => length2-this.referenceDate[2].getUTCDay()+i);
                this.calendarContent = this.calendarContent.concat(preextension);
            }   
            this.calendarContent = this.calendarContent.concat(tmp);
            if(this.calendarContent.length < 49 ) {
               let nr = 49 - this.calendarContent.length;
               this.calendarContent = this.calendarContent.concat(Array.from(Array(nr), (_, i) => i+1));
	       }
	    }

	    private getAllLoggers() {
	    	 this.allloggers = this.taskService.getLoggers();
		 }

		 private getEmA(activity) {
		 let i = 0;
		 for(let a of this.ats) {
		 	if(a == activity)
				return i;
				
				i = i+1;
				}
			
				return 0;
				}
			private saveExtra() {
			let i = 0;
		 for (let em of this.extraModels) {
		 this.loggers[i].activity_type = this.getEmA(em.activity);
		 let td = new Date(this.loggers[i].start_time);
			td.setHours(em.sh);
			td.setMinutes(em.sm);
			this.loggers[i].start_time = td.toString();
			td = new Date(this.loggers[i].end_time);
			td.setHours(em.eh);
			td.setMinutes(em.em);
			this.loggers[i].end_time = td.toString();
			this.loggers[i].comment = em.comment.slice(0,300) + this.loggers[i].comment.slice(300);
			i = i +1;
		 }
	    }

	    private getLoggers() {
	    this.saveExtra();
	    this.loggers = [];
	    this.showExits = [];
	    this.extraModels = [];
	    let extraModel = null;
	    console.log(this.allloggers);
	    console.log(this.selectedDate);
	    for(let logger of this.allloggers) {
	    extraModel = {activity:"Documentation",sh:"00",sm:"00",eh:"00",em:"00",comment:""};
	    let td = new Date(logger.start_time);
	    if((td.getFullYear() == this.selectedDate.getFullYear()) && (td.getMonth() == (this.selectedDate.getMonth())) && (td.getDate() == this.selectedDate.getDate())) {
	    extraModel.activity = this.ats[logger.activity_type];
	    let filler = "0";
	    if((""+td.getHours()).length==1)
	    	extraModel.sh = filler+td.getHours();
		else
		extraModel.sh = ""+td.getHours();
	   if((""+td.getMinutes()).length==1)
	   	extraModel.sm = filler+td.getMinutes();
	   else
		extraModel.sm = ""+td.getMinutes();
		td = new Date(logger.end_time);
		if((""+td.getHours()).length==1)
                extraModel.eh = filler+td.getHours();
                else
                extraModel.eh = ""+td.getHours();
           if((""+td.getMinutes()).length==1)
                extraModel.em = filler+td.getMinutes();
           else
                extraModel.em = ""+td.getMinutes();
		extraModel.comment = logger.comment.slice(0,300);
		if(extraModel.comment.length > logger.comment)
			extraModel.comment = extraModel.comment + "...";
			this.extraModels.push(extraModel);
			this.showExits.push(false);
			this.loggers.push(logger);
			}
			}
			console.log(this.loggers);

	    }    
	    
	    
     constructor(private taskService:TasksService) {
     this.calcDates();
     this.getAts();
     this.getAllLoggers();
     this.getLoggers();
     this.getDefaultTime();

     }

     public dismissLogger(i, logger) {
     this.loggers.splice(i,1);
     this.extraModels.splice(i,1);
	for(let i=0;i<this.allloggers.length;i++)
	{
	if(this.allloggers[i]==logger)
		this.allloggers.splice(i,1);
	}
	this.getLoggers();
	

	}

	private validateLog(logger) {
	let today = new Date();
		this.selectedDate.setHours(today.getHours());
		this.selectedDate.setMinutes(today.getMinutes());
		let td = this.selectedDate;
		td.setMonth(this.selectedDate.getMonth() -1);
		console.log(td);
		console.log(today);
		if((td.getTime() - today.getTime())>1) {
		this.errorMsg = "You can't enter logs for future dates";
		return false;
		}
		if(!logger.name)
		{
			this.errorMsg = 'Name shoud be set!';
			return false;
		}
		if(logger.start_time.length < 12)
		{
			this.errorMsg = 'Start time should be set!';
			return false;
		}
		if(logger.end_time.length < 12) {
			this.errorMsg = 'End time should be set!';
			return false;
			}
			if(!(logger.activity_type >= 0 && logger.activity_type < this.ats.length)) {
			this.errorMsg = 'Activity type is not set correctly!';
			return false;
			}
			if(!(logger.comment.length > 0)) {
			this.errorMsg = 'Comment is mandatory!';
			return false;
			}
		let td1 = new Date(logger.start_time);
		let td2 = new Date(logger.end_time);
		if(td1 > td2) {
			this.errorMsg = 'Please set a positive duration time!';
			return false;
			}

		if((logger.activity_type==1) && ((td2.getTime() - td1.getTime())/(1000*60)) < 15 )
		{
			this.errorMsg = 'Please set a timeframe, that is more, than 15 minutes!';
			return false;
			}
		return true;

	}

	public addLog() {
	let logger = this.taskService.getEmptyTask();
		logger.name = 'John Doe';
		let date = this.selectedDate;
		let tm = this.selectedDate.getMonth() + 1;
		date.setMonth(tm);
		date.setHours(this.times[0]);
		date.setMinutes(this.times[1]);
		logger.start_time = date.getFullYear() + "-";
		if(date.getMonth().toString().length==1)
	       	logger.start_time = logger.start_time+"0";
		logger.start_time = logger.start_time + date.getMonth() + "-";
	        if(date.getDate().toString().length==1)
		logger.start_time = logger.start_time+"0";
		logger.start_time = logger.start_time + date.getDate() + " ";
	        if(date.getHours().toString().length==1)
		logger.start_time = logger.start_time+"0";
		logger.start_time = logger.start_time+date.getHours() + ":";
	        if(date.getMinutes().toString().length==1)
		logger.start_time=logger.start_time+"0";
		logger.start_time = logger.start_time + date.getMinutes();
		console.log(logger.start_time);
		date.setHours(this.times[2]);
		date.setMinutes(this.times[3]);
		logger.end_time = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
		logger.comment = this.newText;
		logger.activity_type = this.ats.indexOf(this.atsv);
		if(this.validateLog(logger)) {
			this.allloggers.push(logger);
			this.getLoggers();
			this.errorMsg = '';
		}
	}

     public getTasks(tile) {
     if(Number(tile) > 0 && Number(tile) <= 31) {
     let date = new Date(this.referenceDate[1].getFullYear() + "-" + (this.referenceDate[1].getMonth()+1) + "-" +tile);
     for(let logger of this.allloggers) {
     let td = new Date(logger.start_time);
     if(td.getFullYear() == date.getFullYear() && td.getMonth() == date.getMonth() && td.getDate() == date.getDate())
     	return true;
	}
	}
	return false;
	}

	public getTimeSummary(tile, isActive) {
	let summary = 0;
		if(Number(tile) > 0 && Number(tile) <= 31) {
     let date = new Date(this.referenceDate[1].getFullYear() + "-" + (this.referenceDate[1].getMonth()+1) + "-" +tile);
     for(let logger of this.allloggers) {
     let td = new Date(logger.start_time);
     if(td.getFullYear() == date.getFullYear() && td.getMonth() == date.getMonth() && td.getDate() == date.getDate())
     {
     	let td1 = new Date(logger.end_time);
	summary = summary + (td1.getTime() - td.getTime())/(1000*60);
     }
     }
     }
     	let hours = Math.trunc(summary/60);
	let minutes = summary - (Math.trunc(summary/60)*60);
	return hours + "h " + minutes + "m";
	}

	private getSummaryTime() {
		let summary = 0;
        for(let logger of this.loggers) {
        let td = new Date(logger.start_time);
                if(this.selectedDate.getFullYear() === td.getFullYear() && (this.selectedDate.getMonth()) == td.getMonth() && this.selectedDate.getDate() == td.getDate()) {
                let td1 = new Date(logger.end_time);
                summary = summary + (td1.getTime() - td.getTime())/(1000*60);
                }
                }
		return summary;
	}

	public getOverallHours() {
		let summary = this.getSummaryTime();
		return Math.trunc(summary/60);
	}

	public getOverallMinutes() {
		let summary = this.getSummaryTime();
		return summary - (Math.trunc(summary/60)*60);
	}

     public setSelectedDate(sd, isActive) {
     if(isActive) {
     this.selectedDate = new Date(this.referenceDate[1].getFullYear() + " " + (this.referenceDate[1].getMonth()+1) + " " +sd);}
     this.getLoggers();
     this.getDefaultTime();
     }

     private resetCardTimePart(i,type,duration) {
     switch(type) {
     case 'sh': {
     	if(this.extraModels[i].sh >= 0 && this.extraModels[i].sh <= 23)
     		this.extraModels[i].sh = this.extraModels[i].eh - 2;
		else 
		this.extraModels[i].sh = "00";
     	break;
	}
	case 'sm': {
		if(this.extraModels[i].sm >= 0 && this.extraModels[i].sm <= 60)
			this.extraModels[i].sm = this.extraModels[i].em - duration;
		else
			this.extraModels[i].sm = "00";
		break;
		}
		case 'eh':{
		 if(this.extraModels[i].eh >= 0 && this.extraModels[i].eh <= 23)
                	this.extraModels[i].eh = this.extraModels[i].eh + 2;
        	else
			this.extraModels[i].eh = "00";
		break;
		}
		case 'em':{ 
		if(this.extraModels[i].em >= 0 && this.extraModels[i].em <= 60)
                        this.extraModels[i].em = this.extraModels[i].em + duration;
                else
			this.extraModels[i].em = "00";
		break;
		}
	}
     }

     public changeTimeEntry(i, type) {
	let td = this.selectedDate;
	td.setHours(this.extraModels[i].sh);
	td.setMinutes(this.extraModels[i].sm);
	let td2 = this.selectedDate;
	td2.setHours(this.extraModels[i].eh);
	td2.setHours(this.extraModels[i].em);
	let duration = (td2.getTime() - td.getTime())/(60000);
	if(duration < 1 || ((this.loggers[i].activity_type==1) && (duration<15)))
		this.resetCardTimePart(i,type, 20);
	this.getLoggers();
     }

     public getSelection(tile, index, isActive) {
     	if(isActive)
	{
	if(tile == this.selectedDate.getDate())
	{
		return 'selected';
		} else 
		return 'unselected';
	}
	}

	public getNrOfTasks(tile, isActive) {
	let tasks = 0;
		if(isActive) {
			let date = new Date(this.referenceDate[1].getFullYear() + "-" + (this.referenceDate[1].getMonth()+1) + "-" + tile);
			for(let logger of this.allloggers) {
				let logTime = new Date(logger.start_time);
				if(logTime.getFullYear() == date.getFullYear() && logTime.getMonth() == date.getMonth() && logTime.getDate() == date.getDate()) {
				tasks = tasks+1;
				}
				}
				}
				return tasks;
				}

				public getPeopleForTasks(tile, isActive) {
				let people = [];
				if(isActive) {
				 let date = new Date(this.referenceDate[1].getFullYear() + " " + this.referenceDate[1].getMonth() + " " + tile);
				 for(let logger of this.loggers) {
                                let logTime = new Date(logger.start_time);
				if(logTime.getFullYear() == date.getFullYear() && logTime.getMonth() == date.getMonth() && logTime.getDate() == date.getDate()) {
				let i = 0;
				for(let person of people) {
					i = i+1;
					if(person == logger.name)
						break;
						if(i == people.length) {
					people.push(logger.name);
				}
				}
				}
				}
				}
				return people.length;
				}
     public isToday(tile) {
     let date = new Date();
       if(date.getDate() == tile) {
       	return true;
	}
	else return false;
     }

     private getDefaultTime() {
     if(this.loggers.length>0) {
     	let td1 = new Date(this.loggers[this.loggers.length-1].start_time);
	let td2 = new Date(this.loggers[this.loggers.length-1].end_time);
	this.times[0] = td2.getHours();
	this.times[1] = td2.getMinutes();
	this.times[2] = td2.getHours() + 2;
	this.times[3] = td2.getMinutes();
     } else {
     let date = new Date();
     if(date.getHours()>=4)
     this.times[0] = date.getHours() - 4;
     else
     this.times[0] = 0;
	this.times[1] = date.getMinutes();
	this.times[2] = date.getHours();
	this.times[3] = date.getMinutes();
	}
	}

	public calcDuration(t1, t2, p) {

	let tmp1 = new Date(t1);
	let tmp2 = new Date(t2);
	if(tmp1>tmp2){
	if(p=='h')
		return Math.floor((tmp1.getTime() - tmp2.getTime()) / (1000 * 60 * 60));
	else if(p=='m')
	return Math.trunc((tmp1.getTime() - tmp2.getTime()) / (1000 * 60)) - (Math.floor((tmp1.getTime() - tmp2.getTime())/(1000*60*60) )*60);
	}else
		return 0;
	}

	ngOnInit() {
    }

    public switchStyle(e) {
       this.viewStyle = e;
       }

       public incTime(nr, p) {
       if((p==1 && this.times[nr]==23) || (p==2 && this.times[nr]==60))
        this.times[nr] = 0;
 	else this.times[nr] = this.times[nr]+1;
	  }

	  public decTime(nr, p) {
	  if(p==1 && this.times[nr]==0)
	     this.times[nr] = 23;
	     else if(p==2 && this.times[nr]==0)
	     this.times[nr] = 60;
	     else
	   this.times[nr] = this.times[nr]-1;
	  }

       public getClassOfTile(tilenr, index) {
          if (index<14 && tilenr>20)
	  return 'inactive';
	  else if(index>30 && tilenr<20)
	  return 'inactive';
	  else
	  return 'active';
	  }

       public travelDate(direction) {
	  if (this.viewStyle=='m') {
		  this.referenceDate[0] = new Date(this.referenceDate[0].getFullYear(), this.referenceDate[0].getMonth()+(direction*1), 1);
		this.calcDates();
	    }
	    }
	    public getAts() {
	    this.ats = this.taskService.getActivityTypes();
	    }

}
