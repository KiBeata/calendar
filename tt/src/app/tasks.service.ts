import { Injectable } from '@angular/core';
import { Task } from './tasks/task';
import { Taskers, ActivityTypes } from './tasks/taskers';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
    private tasks: Task[] = [];
    private summary: Task;
    private activity_types: string[] = [];

    constructor() { 
    this.tasks = Taskers;
    console.log(this.tasks);
	this.activity_types = ActivityTypes;
	}

	private calcSummary() {
	let duration = 0;
	if(this.tasks.length>0) {
	for(let task in this.tasks) {
	//let spent = new Date(task.end_time).valueOf() - new Date(task.start_time).valueOf();
	//duration = duration + spent;

	   }
	   }
	}


    public getLoggers() {
     return this.tasks;   
     }

    public getSummaryOfTasks() {
      this.calcSummary();
      return this.summary;
      }

      public getEmptyTask() {
      	return new Task();
      }

     public getActivityTypes() {
       return this.activity_types;
    }
}
