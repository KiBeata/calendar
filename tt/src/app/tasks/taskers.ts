import { Task } from './task';


export const Taskers:Task[] = [
{ 
	name: "John Doe",
	activity_type: 1,
	start_time: "2020-06-20 11:12",
	end_time: "2020-06-20 14:00",
	comment: "Initial project setup"

},
{ 
     name: "John Doe",
     activity_type:2,
     start_time:"2020-06-20 16:00",
     end_time:"2020-06-20 18:00",
     comment: "Some additional things"
},
{ 
    name: "John Doe",
    activity_type:2,
    start_time:"2020-06-22 09:00",
    end_time:"2020-06-22 11:00",
    comment:"SSSS"

    },
    {
    name: "John Doe",
    activity_type:2,
    start_time:"2020-06-26 10:00",
    end_time:"2020-06-26 11:00",
    comment:""
    }
    ];

export const ActivityTypes:string[] = [
"Development",
"Meeting",
"Documentation",
"Testing",
"Environment Setup",
"Design",
"Research"
];
